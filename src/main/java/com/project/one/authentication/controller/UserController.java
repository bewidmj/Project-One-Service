package com.project.one.authentication.controller;


import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.one.authentication.model.User;
import com.project.one.authentication.repository.UserRepository;
import com.project.one.authentication.service.EncryptionService;

@RestController
@RequestMapping("/user")
public class UserController {
	 
	@Autowired
	 UserRepository userRepository;
	
	@Autowired
	 EncryptionService encryptionService;
	
	 @CrossOrigin
	 @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
	 public ResponseEntity<?> login(@RequestParam String mobileNo, @RequestParam String passWord){
	  User user = userRepository.findOne(mobileNo);
	  user.setPassWord(EncryptionService.decrypt(user.getPassWord(), user.getMobileNo()));
	  if(user.getMobileNo().equals(mobileNo) && user.getPassWord().equals(passWord)){
		  user.setPassWord(null);
		  user.setDate(null);
		  return  new ResponseEntity<User> (user, HttpStatus.OK);
	  }
	  else {
	  return  new ResponseEntity<>(new String ("{\"message\": \"Mobile number or Password does not match\"}"), HttpStatus.BAD_REQUEST);  
	  }
	 }
	 
	 /**
	  * POST /create  --> Create a new User and save it in the database.
	  */
	 @CrossOrigin
	 @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
	 public ResponseEntity<?> create(@RequestBody User user) {
	  user.setBusinessRole("User");
	  user.setDate(new Date());
	  user.setPassWord(EncryptionService.encrypt(user.getPassWord(), user.getMobileNo()));
	  userRepository.save(user);
	  return  new ResponseEntity<>(new String ("{\"message\": \"Registered Successfully\"}"), HttpStatus.OK);
	 }
	 
	 /**
	  * POST /read  --> Read a User by User name from the database.
	  */
	 @RequestMapping(value = "/read", method = RequestMethod.POST, produces = "application/json")
	 public ResponseEntity<String> read(@RequestParam String mobileNo){
	  User user = userRepository.findOne(mobileNo);
	  return  new ResponseEntity<> (user.getFirstName(), HttpStatus.OK);
	 }
	 
	 /**
	  * POST /update  --> Update a user data and save it in the database.
	  */
	 @RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json")
	  public ResponseEntity<String> update(@RequestParam String mobileNo, @RequestParam String firstName){
	  User user = userRepository.findOne(mobileNo);
	  user.setFirstName(firstName);
	  userRepository.save(user);
	  return  new ResponseEntity<> (user.getFirstName(), HttpStatus.OK);
	 }
	 
	 /**
	  * POST /delete  --> Delete a User from the database.
	  */
	 @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = "application/json")
	  public ResponseEntity<String> delete(@RequestParam String mobileNo){
	  userRepository.delete(mobileNo);
	  
	  return  new ResponseEntity<> ("User deleted successfully", HttpStatus.OK);
	 }
	 
	 /**
	  * POST /read  --> Read all Users from the database.
	  */
	 @RequestMapping(value = "/read-all", method = RequestMethod.POST, produces = "application/json")
	 public ResponseEntity<User> readAll(){
	  List<User> users = userRepository.findAll();
	  return  new ResponseEntity<User> (users.get(0), HttpStatus.OK);
	 }

}
