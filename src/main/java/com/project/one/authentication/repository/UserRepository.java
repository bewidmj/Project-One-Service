package com.project.one.authentication.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.project.one.authentication.model.User;

@Transactional
public interface UserRepository extends MongoRepository<User, String>  {

	public List<User> findByMobileNo(String mobileNo);

}
